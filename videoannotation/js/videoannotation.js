/*
  Assumes jQuery and the following API:
  
  video.GetTimeCode()
  intToCode(timecode_int) returns timecode_text

  Components: pause,getTimeCode,play
  clipper for annotation field:
     @param video_interface
     @param version_id (annotation node should take care of this)
     @param callback to set start,end,moreData
*/

//to store {nid-fieldname:type} pairs

if (typeof(VideoAnnotator) == 'undefined') {

    function VideoAnnotator() {
	this.__NAME__='VideoAnnotator';
	this.potential_annotatees = {};
	this.tracker = {};
	//var videoannotation_tracker = {};
    }
}

VideoAnnotator.prototype.start = function(annID, mode, dom, options, data, form) {
    var self = this;
    switch (mode) {
    case 'create':
    case 'edit':
	self.tracker[annID] = new Clipper(annID, dom, options, data, form);
	break;
	// 3. roll up the second 'set end'? (optional for now?, since not in HHP comps)
    case 'view':
	//mode==view/create/edit
	// 4. make clip bar (JS jumps to start and plays when clicking the internal div)
	self.tracker[annID] = new Clipper(annID, dom, options, data);
	break;
    }

}

VideoAnnotator.prototype.stop = function(annid) {
    var self = this;
    self.tracker[annID].destroy();

}

VideoAnnotator.prototype.getValues = function(annid) {

}

VideoAnnotator.prototype.update = function(annid, data) {

}

///@return boolean
VideoAnnotator.prototype.annotatable = function(dom) {
    var obj_elts = jQuery('object,embed',dom);
    for (a in Clipper.clippers) {
	//earlier jQuery doesn't filter with a function :-(
	//var ok_obj_list = obj_elts.filter(Clipper.clippers[a].annotatable);
	var ok_obj_list = jQuery.map(obj_elts, Clipper.clippers[a].annotatable);
	if (ok_obj_list.length) {
	    return {'name':a,'objs':ok_obj_list};
	}
    }
    return false;
}

/*schema(types)
  @param types object with types.dict, types.string, types.arrayOf, etc.
  @return {
      'form':form obj (maybe by classes
      'dict':obj with types.* as pointers with vars
      'indexable_fields':array of strings in dict that are indexable
  }
*/
VideoAnnotator.prototype.schema = function(types) {

}


/************************************************
 *
 * INTERNAL FUNCTIONS
 *
 ************************************************/

function Clipper(annID, atee_dom, options, data, annotator_form) {
    //needs to pass which item in video, or get it back here from the theme
    var self = this;
    //logDebug(annotator_form, 'Clipper');
    this.annID = annID;
    this.video = self.getVideo(atee_dom, options);
    //TODO:need to throw error if we can't clip.
    this.data = data;
    this.form = false;

    if (annotator_form) {
	//logDebug('add Clipper Listeners');
	this.form = annotator_form;
	this.startField = jQuery('.videoclipStartTime', annotator_form).get(0);
	this.endField = jQuery('.videoclipEndTime', annotator_form).get(0);
	this.startButton = jQuery('.videoclipStartButton',annotator_form).get(0);
	this.endButton = jQuery('.videoclipEndButton',annotator_form).get(0);
	this.durationField = jQuery('.videoclipDuration',annotator_form).get(0);
	
	//funny syntax to keep 'this' working within start/end
	jQuery(this.startButton).bind('click', function(e){self.start(e,this);});
	jQuery(this.endButton).bind('click', function(e){self.end(e,this);});
    }
    /*Clip Strip*/
    this.clipStrip = new ClipStrip(this.video);

    if (data && typeof(this.data.starttime) === 'number') {
	self.video.SetTime(self.data.starttime);
	if (typeof(this.data.endtime) === 'number') {
	    self.video.PauseAt(self.data.endtime);
	}

	var w1 = new WaitUntil(this.video.durable, 50, this.video);
	w1.addListener(function() {self.updateClipStrip();});
    }



}

Clipper.clippers = {}; //allows clippers to register here

Clipper.prototype.destroy = function(annID) {
    if (this.form) {
	//important so the form can be reused for another annotatee
	jQuery(this.startButton).unbind('click',this.start);
	jQuery(this.endButton).unbind('click',this.end);
	//for avoiding memory leaks
	delete this.startButton;
	delete this.endButton;
	delete this.startField;
	delete this.endField;
	delete this.durationField;
	delete this.form;
    }
}

Clipper.prototype.start = function(evt) {
    var self = this;
    self.video.Pause();
    var tc = self.video.GetTime(); //doing nothing atm
    self.startField.value = self.video.GetTimeCode();
    self.durationField.value = self.video.GetDuration();

    //end should always be >= startTime
    var end_tc = Clipper.codeToInt(self.endField.value);
    if (tc > end_tc) {
	self.endField.value = self.startField.value;
	end_tc = tc;
    }
    self.clipStrip.update(tc,end_tc);
}

Clipper.prototype.end = function(evt) {
    var self = this;
    self.video.Pause();
    var tc = self.video.GetTime(); //doing nothing atm
    self.endField.value = self.video.GetTimeCode();
    self.durationField.value = self.video.GetDuration();
    
    var start_tc = Clipper.codeToInt(self.startField.value);
    if (tc < start_tc) {
	self.startField.value = self.endField.value;
	start_tc = tc;
    }
    self.clipStrip.update(start_tc,tc);
}

Clipper.prototype.getVideo = function(atee_dom, options) {
    var ok_clipper = VideoAnnotator.prototype.annotatable(atee_dom);
    return new Clipper.clippers[ok_clipper.name](ok_clipper.objs[0].id);
}

Clipper.prototype.updateClipStrip = function() {
    var self = this;
    var cur_data = self.getData();
    self.clipStrip.positionStrip();
    self.clipStrip.update(cur_data.starttime,cur_data.endtime);
}

Clipper.prototype.getData = function() {
    var self = this;
    if (self.form) {
	return {starttime:Clipper.codeToInt(self.startField.value),
		endtime:Clipper.codeToInt(self.endField.value)
	       };
    } else {
	return self.data;
    }
}

Clipper.prototype.inMovieTime = function() {
    
}

///NOT prototypes
Clipper.intToCode = function(seconds) {
    //second argument is the timecode object to be modified, otherwise it'll create one
    var tc = {};
    intTime = Math.floor(seconds);
    tc.hr = parseInt(intTime / 3600);
    tc.min = parseInt((intTime % 3600)/60);
    tc.sec = intTime % 60;
    tc.fraction = seconds - intTime;

    if (tc.hr  < 10) {tc.hr  = "0"+tc.hr; }
    if (tc.min < 10) {tc.min = "0"+tc.min; }
    if (tc.sec < 10) {tc.sec = "0"+tc.sec; } 
    var suffix = '';
    if (tc.fraction) { //puts it in form ':00.0'
	var front = Math.floor(100*tc.fraction);
	var back = Math.floor(10 * (tc.fraction  - Math.floor(100*tc.fraction)));
	suffix = ':'+((front<10)?'0'+front:front)+ '.' + back;
    }
    return tc.hr + ":" + tc.min + ":" + tc.sec + suffix;
}
    
Clipper.codeToInt = function(code) {
    var mvscale = 1;
    //takes a timecode like '0:01:36:00.0' and turns it into # seconds
    var t = code.split(':');
    var x = t.pop();
    var seconds = 0;
    if (x.indexOf('.') >= 0) { //00.0 format is for frames
	//ignore frames
	x=parseInt(t.pop());
    }
    var timeUnits = 1; //seconds -> minutes -> hours
    while (x || t.length > 0) {
	seconds += x * timeUnits * mvscale;
	timeUnits *= 60; 
	x=parseInt(t.pop(),10);
    }
    return seconds;
}

/* WaitUntil: helper object to keep trying something until it works
   @param func a function that keeps getting run until no errors are
               thrown
*/
function WaitUntil(func,msec,funcself, arg) {
    var self = this;
    this.fired = false;
    this.listeners = [];
    var timeID;
    var pulser = function() {
	if (!self.fired) {
	    try {
		//logDebug(typeof(func));
		var v = func.call(funcself, arg);
		self.fired = true;
		//logDebug('waituntil success');
		self.trigger();
	    }
	    catch(err) {
		timeID = window.setTimeout(arguments.callee,msec);
		//logDebug('error is special',err);
	    }

	}
	else {
	    //logDebug('clearTimeout');
	    window.clearTimeout(timeID);
	    self.trigger('complete');
	}
    };
    timeID = window.setTimeout(pulser,0);
    return self;
}

WaitUntil.prototype.trigger = function() {
    var self = this;
    var t;
    //logDebug('trigger');
    while (t = self.listeners.pop()) {
	t.trigger('complete');
    }
}
    
WaitUntil.prototype.addListener = function(func) {
    var self = this;
    if (self.fired) {
	//logDebug('already fired');
	func();
    }
    else {
	self.listeners.push(jQuery(self).bind('complete',func));
	//logDebug('bind');

    }
}

/**********
 * display strip underneath time line.
 * This is mostly stolen inspired from (my code in) the VITAL project
 *********/
function ClipStrip(video_interface) {
    this.video = video_interface;

    //connected to the width of the bookend*.gif files in the styling
    this.CLIP_MARKER_WIDTH=7;

    this.create();
}
    
ClipStrip.prototype.create = function() {
    var self = this;
    var movie_dom = self.video.movie();
    self.dom = jQuery('<div class="clipStrip"><div class="clipStripLabel"><!-- nothing --></div><div class="clipStripTrack"><div class="clipStripStart clipSlider" style="display:none"></div><div class="clipStripRange" style="display:none"></div><div class="clipStripEnd" style="display:none"></div></div></div>');

    jQuery(movie_dom).after(self.dom);

    self.clipStripTarget = jQuery('.clipStripTrack',self.dom).get(0);
    self.clipStripStart = jQuery('.clipStripStart',self.dom).get(0);
    self.clipStripRange = jQuery('.clipStripRange',self.dom).get(0);
    self.clipStripEnd = jQuery('.clipStripEnd',self.dom).get(0);

    //temporary value
    self.clipStripLength = parseInt(self.clipStripTarget.offsetWidth,10);

    self.positionStrip();
}

ClipStrip.prototype.clipStripPos = function(timeCodeInteger) {
    var self = this;
    try {
	self.movDuration = self.video.GetDuration();
    } catch(err) {/*who cares?*/}
    var ratio = self.clipStripLength/self.movDuration;
    return ratio * timeCodeInteger;
}

ClipStrip.prototype.positionStrip = function() {
    var self = this;
    var dim = self.video.TimeStrip();

    jQuery(self.dom)
    .css('padding-right',dim.x+'px')
    .width(dim.w+'px');

    jQuery('.clipStripTrack',self.dom)
    .css('left',(dim.x)+'px')
    .width(dim.w+'px');

    self.clipStripLength = dim.w;
}

ClipStrip.prototype.update = function(intStartTime, intEndTime) {
    var self = this;
    left = self.clipStripPos(intStartTime);
    right = self.clipStripPos(intEndTime);

    self.clipStripStart.style.left = parseInt(left-self.CLIP_MARKER_WIDTH+2,10)+'px';
    self.clipStripRange.style.left = left+'px';
    self.clipStripRange.style.width = parseInt(right-left,10)+'px';
    self.clipStripEnd.style.left = right+'px';
    
    self.clipStripStart.style.display = 'block';
    self.clipStripRange.style.display = 'block';
    self.clipStripEnd.style.display = 'block';
}

ClipStrip.prototype.hideClipStrip = function () {
    var self = this;
    
    self.clipStripStart.style.display='none';
    self.clipStripRange.style.display='none';
    self.clipStripEnd.style.display='none';
}


/************************************************
 *
 * INITIALIZATION
 *
 ************************************************/

//REGISTER
if (typeof(AnnotationController) != 'undefined') {
    //this might be clobbering.  should do this more conditionally
    AnnotationController.annotators['videoannotation'] = new VideoAnnotator();
}
