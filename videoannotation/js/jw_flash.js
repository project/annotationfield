/*
  support for the jw_flv player.  documentation at:
  http://www.jeroenwijering.com/extras/javascript.html
  http://www.jeroenwijering.com/extras/readme.html
  dependency: Clipper()
*/

function JWclipper(obj_id, target) {
    this.obj_id = obj_id;
    this.counter = 0;
}

JWclipper.jw_state = {};
JWclipper.jw_listeners = {};
JWclipper.getItemData = function(obj_id) {
    //note this is also an event that a new item was selected.
    try {
	var jwObj = JWclipper.jw_state[obj_id];
	jwObj.itemData = JWclipper.thisMovie(obj_id).itemData(jwObj.item);
	//window.console.log(jw_state[obj_id]);
    } catch(e) {
	//window.console.log('getItemData error',e);
	/*probably not loaded yet. WHAT TODO?*/
    }
}
JWclipper.dispatchItemGet = function(obj_id) {
    var f = function() {JWclipper.getItemData(obj_id);};
    setTimeout(f,100);
}

JWclipper.annotatable = function(obj) {
    var yes = false;
    switch (obj.tagName.toLowerCase()) {
    case 'embed':
	var src = obj.getAttribute('src');
	yes = /mediaplayer\.swf/.test(src);
	break;
    case 'object':
	var movieParam = function(o){return (o.name=='movie')?o.value:null;};
	var movie = jQuery.map(obj.getElementsByTagName('param'),movieParam);
	if (movie.length) {
	    yes = /mediaplayer\.swf/.test(movie);
	}
	break;
    }
    //used in a jQuery.map() call
    return (yes)?obj:null;
}

JWclipper.thisMovie = function(movieName) {
    //logDebug(movieName);
    if (typeof(document[movieName]) != 'undefined') {
	return document[movieName];
    } else {
	return window[movieName];
    }
    /*
    if(navigator.appName.indexOf("Microsoft") != -1) {
	return window[movieName];
    } else {
	return document[movieName];
    }
    */
}

///for JWclipper instances
JWclipper.prototype.movie = function() {
    return JWclipper.thisMovie(this.obj_id);
}

JWclipper.prototype.item = function() {
    return JWclipper.jw_state[this.obj_id];
}

JWclipper.prototype.GetTime = function() {
    return JWclipper.jw_state[this.obj_id].time;
}
JWclipper.prototype.GetDuration = function() {
    var item = this.item();
    return item.time+item.time2;
}

JWclipper.prototype.GetTimeCode = function() {
    return Clipper.intToCode(this.GetTime());
}

JWclipper.prototype._PlayPause = function(play) {
    var item = this.item();
    if (typeof(item) == 'undefined' || item.state != play) {
	this.movie().sendEvent('playpause');
    }
}
JWclipper.prototype.Play = function() {
    this._PlayPause(1);
}
JWclipper.prototype.Pause = function() {
    this._PlayPause(0);
}

JWclipper.prototype.LoadAt = function(startseconds) {
    var self = this;
    var settime = function () {
	self.Pause();
	self._SetTime(startseconds); //second time will shift from last hinted frame to real code
    };
    var scrubWait = function () {
	self.Pause(startseconds);
	var w3 = new WaitUntil(self.scrubbed, 50, self, startseconds);
	w3.addListener(settime);
    }
    var loadWait = function() {
	self.Play();
	var w2 = new WaitUntil(self.scrubable, 50, self, startseconds);
	w2.addListener(scrubWait);
    }
    var w1 = new WaitUntil(self.loaded, 50, self);
    w1.addListener(loadWait);
}

JWclipper.prototype.loaded = function() {
    var self = this;
    var video = this.movie();
    if (typeof(video.sendEvent) != 'function') {
	throw "SWF not loaded";
    }
    var item = this.item();
    if (typeof(item) == 'undefined') {
	throw "state not available";
    }
    return true;
}

JWclipper.prototype.scrubable = function(secs) {
    var self = this;
    if (!self.loaded()) {
	throw "not even loaded yet";
    }
    var item = this.item();
    if (item.state ==0) {
	throw "state is still stopped (possibly loading video)";
    }
    //logDebug('time2:'+item.time2);
    if (item.time2 <= 0) {
	///&& item.load/100 < secs/(item.time+item.time2)) {
	/*this is problematic for big videos
	 *we should not stop seeking before this
	 *we could, however
	 */
	throw "not enough buffer loaded";
    }
    return true;
}

JWclipper.prototype.scrubbed = function(secs) {
    var self = this;
    if (!self.loaded()) {
	throw "not even loaded yet";
    }
    var item = this.item();
    //logDebug('time:'+item.time+' secs:'+secs);
    self._SetTime(secs);
    if (item.time < secs-10) {
	throw "not yet scrubbed";
    }
    return true;
}

///valid duration is available
JWclipper.prototype.durable = function() {
    var self = this;
    if (self.GetDuration() <= 0) {
	throw "not long enough!";
    }
    return true;
}

JWclipper.prototype._SetTime = function(seconds) {
    var movie = this.movie();
    movie.sendEvent('scrub',seconds);
}
JWclipper.prototype.SetTime = function(seconds, endtime) {
    var self = this;
    try {
	if (self.scrubable()) {
	    self._SetTime(seconds);
	}
    }
    catch(e) {
	self.LoadAt(seconds);
    }
}

JWclipper.prototype.PauseAt = function(seconds) {
    var self = this;
    self._Listen(function(typ,pr1,pr2) {
	if (typ == 'time' && pr1 > seconds) {
	    self.Pause();
	    return true;
	}
	return false;
    });
}

JWclipper.prototype._Listen = function(func) {
    var self = this;
    var c = ++self.counter;
    JWclipper.jw_listeners[self.obj_id+'_'+c] = function(typ,pr1,pr2,pid)
    {
	return ((pid === self.obj_id)?func(typ,pr1,pr2):false);
    }
}

JWclipper.prototype.TimeStrip = function() {
    var movie = this.movie();
    var width = movie.width-160+20;
    var re = /usefullscreen=true/;
    if (re.test(movie.innerHTML) || re.test(movie.lastChild.outerHTML)) {
	width -= 20;
    }
    return {
	//w:265,
	w:width,
	x:59,
	visible:true
    };
    
}

//REGISTER with Clipper
Clipper.clippers['jw'] = JWclipper;

/* getUpdate() Magic function name called by the flash object
   @param typ type of update [time, volume, item, state, load (%loaded), size(w,h)]
   @param pr1 
   @param pr2 if typ==time, then pr2 is time left until the end
              if typ==size, then pr2 is height
   @param pid if there's more than one object elt, what the object ID is
*/

function getUpdate(typ,pr1,pr2,pid) {
    //window.console.log('getUpdate',pid,typ,pr1,pr2);
    if (typeof(JWclipper.jw_state[pid]) == 'undefined') {
	JWclipper.jw_state[pid] = {};
    }
    JWclipper.jw_state[pid][typ] = pr1;
    if (typeof(pr2) == 'number') {
	JWclipper.jw_state[pid][typ+'2'] = pr2;
    }
    //should be last just in case it's slow
    if (typ == 'item') {
	//so the flash doesn't get caught up in two JS functions at once.
	JWclipper.dispatchItemGet(pid);
    }
    for (a in JWclipper.jw_listeners) {
	if (JWclipper.jw_listeners[a](typ,pr1,pr2,pid)) {
	    delete JWclipper.jw_listeners[a];
	}
    }
}

