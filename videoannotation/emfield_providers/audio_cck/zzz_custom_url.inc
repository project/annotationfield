<?php

function _audio_cck_zzz_custom_url_default_types() {
  return array('mp3', 'rm');
}

function audio_cck_zzz_custom_url_info() {
  $name = t('Custom URL');
  $features = array(
    array(t('Thumbnails'), t('No'), ''),
    array(t('Autoplay'), t('Yes'), ''),
    array(t('RSS attachment'), t('No'), ''),
  );
  return array(
    'provider' => 'zzz_custom_url',
    'name' => $name,
    'url' => '',
    'settings_description' => t('These settings specifically affect audios displayed from custom URL\'s. When a field uses a URL it determines to be a link directly to a audio file, it will embed that file into the content.'),
    'supported_features' => $features,
    'weight' => 9,
  );
}

function audio_cck_zzz_custom_url_settings() {
  $options = array(
    'mp3' => t('MP3 (mp3)'),
    'wma' => t('Windows Media (wma)'),
    'mov' => t('Quicktime (mov)'),
    'rm' => t('Real Media (rm)'),
  );
  $form = array();
  $form['audio_cck_zzz_custom_url_supported_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Supported Types'),
    '#options' => $options,
    '#default_value' => variable_get('audio_cck_zzz_custom_url_supported_types', _audio_cck_zzz_custom_url_default_types()),
    '#description' => t('Select the audio types you wish to support. When a custom url with that type is entered into an embedded audio field, it will be parsed and displayed appropriately. If a type is not supported, then it will be ignored.'),
  );
  return $form;
}

function _audio_cck_zzz_custom_url_implode_types() {
  return implode('|', variable_get('audio_cck_zzz_custom_url_supported_types', _audio_cck_zzz_custom_url_default_types()));
}

function audio_cck_zzz_custom_url_extract($embed = '') {
  $types = _audio_cck_zzz_custom_url_implode_types();
  if (preg_match('@\.(' . $types . ')@i', $embed, $matches)) {
    return $embed;
  }
  return false;
}

function audio_cck_zzz_custom_url_data($field, $item) {
  $data = array();
  $types = _audio_cck_zzz_custom_url_implode_types();
  $regex = '@\.(' . $types . ')@i';
  if (preg_match($regex, $item['embed'], $matches)) {
    $data['type'] = $matches[1];
  }
  return $data;
}

function audio_cck_zzz_custom_url_embedded_link($audio_code) {
  return $audio_code;
}

function theme_audio_cck_zzz_custom_url_embedded_audio($type, $url, $width, $height, $autoplay = false, $field = NULL, $item = NULL) {
  static $idnum = 0;
  if ($url) {
    $id = $field['field_name'] . '_audio_'.($idnum++);
    switch ($type) {
      case 'wmv':
      case 'wma':
        $autostart = $autoplay ? '1' : '0';
        return '<embed src="' . $url . '" width="' . $width . '" height="' . $height . '" autostart="' . $autostart . '" showcontrols="1" type="application/x-mplayer2" pluginspage="http://www.microsoft.com/windows/windowsmedia/download/"> </embed>';
      case 'mov':
        $autostart = $autoplay ? 'true' : 'false';
        return '<embed src="' . $url . '" width="' . $width . '" height="' . $height . '" autoplay="' . $autostart . '" controller="true" type="video/quicktime" scale="tofit" pluginspage="http://www.apple.com/quicktime/download/"> </embed>';
      case 'rm':
        $autostart = $autoplay ? 'true' : 'false';
        return '<embed type="audio/x-pn-realaudio-plugin" src="' . $url . '" width="' . $width . '" height="' . $height . '" autostart="' . $autostart . '" controls="imagewindow" nojava="true" console="c1183760810807" pluginspage="http://www.real.com/"></embed><br><embed type="audio/x-pn-realaudio-plugin" src="' . $url . '" width="' . $width . '" height="26" autostart="' . $autostart . '" nojava="true" controls="ControlPanel" console="c1183760810807"> </embed>';
      case 'mp3':
        $autostart = $autoplay ? 'true' : 'false';
	/// jw replacement?
	/// http://jssoundkit.sourceforge.net/
	/// this is BSD licensed and includes a getPosition(), etc interface.
	
	$flashplayer = base_path(). variable_get('jeroenwijering_mediaplayer_swf', variable_get('file_directory_path', 'files') . '/mediaplayer.swf');
      	$width = '290';
	$height = '54';
	$args = "file=$url&autostart=$autostart&enablejs=true&javascriptid=$id&type=mp3";
	$rv = "\n";
	$rv .= '<div id="wrapper-'. $id .'">';
	$rv .= "<!--[if !IE]> <-->";
	$rv .= '<object id="'.($id).'" type="application/x-shockwave-flash" width="'. $width .'" height="'. $height .'"
                        data="'. $flashplayer .'">';
	$rv .= '<param name="movie" value="' . $flashplayer . '" />
                        <param name="allowScriptAccess" value="always" />
                        <param name="quality" value="high" />
                        <param name="flashvars" value="'.$args.'" />' . "\n
                        <p>". t('Your browser is not able to display this multimedia content.') .'</p>
                        </object>';
	$rv .= $extra;
	$rv .= '<!--><![endif]-->';
	$rv .= '</div>';

	/* Holy SHIT!!! IE SUX, RLLY!!!!
	 * http://kb.adobe.com/selfservice/viewContent.do?externalId=kb400730&sliceId=2
         * going to the javascript means the flash doesn't have 'activated' first
	 */
	$rv .= '<!--[if IE]><form></form>';

	$rv .= '<script type="text/javascript">    ';
	$rv .= "\nvar so = new SWFObject('$flashplayer','$id','$width','$height','7');";
	$rv .= "so.addParam('allowfullscreen','false');";
	$rv .= "so.addParam('allowScriptAccess','true');";
	$rv .= "so.addVariable('enablejs','true');";
	$rv .= "so.addVariable('usefullscreen','false');";
	$rv .= "so.addVariable('type','mp3');";

	$rv .= "so.addVariable('width','$width');";
	$rv .= "so.addVariable('height','$height');";
	$rv .= "so.addVariable('file','$url');";
	$rv .= "so.addVariable('javascriptid','$id');";
	$rv .="so.write('wrapper-$id');</script>\n";
	$rv .="<![endif]-->";
	return $rv;
    }
  }
}

function audio_cck_zzz_custom_url_thumbnail($field, $item, $formatter, $node, $width, $height) {
  return '';
}

function audio_cck_zzz_custom_url_audio($code, $width, $height, $field, $item, $autoplay) {
  $type = $item['data']['type'];
  $output = theme('audio_cck_zzz_custom_url_embedded_audio', $type, $code, $width, $height, $autoplay, $field, $item);
  return $output;
}

function audio_cck_zzz_custom_url_preview($code, $width, $height, $field, $item, $autoplay) {
  $type = $item['data']['type'];
  $output = theme('audio_cck_zzz_custom_url_embedded_audio', $type, $code, $width, $height, $autoplay, $field, $item);
  return $output;
}
