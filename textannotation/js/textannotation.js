/*
  Assumes jQuery and Marginalia (in geof/ directory)
  
*/

//to store {nid-fieldname:type} pairs

if (typeof(TextAnnotator) == 'undefined') {

    function TextAnnotator() {
	this.__NAME__='TextAnnotator';
	this.tracker = {};
    }
}

TextAnnotator.prototype.start = function(annID, mode, dom, options, data, form) {
    var self = this;

    var post = self.tracker[annID] = new PostMicro(dom);
    //override geof/annotation.js functions called in PostMicro.prototype.showHighlight_Recurse
    _hoverAnnotation = function(){};
    _unhoverAnnotation = function(){};

    switch (mode) {
    case 'create':
    case 'edit':
      self.fields = self._getFormFields(form);
      jQuery(self.fields.grab).bind('click',
	function(e){
	  var annotation = TextAnnotator._createAnnotation(e,this,post);
	  if(annotation) {
	    self.fields.quote.value = annotation.quote;
	    self.fields.start.value = annotation.range.start;
	    self.fields.end.value = annotation.range.end;
	  }
	}
      );

      //self.tracker[annID] = new Clipper(annID, dom, hint, data, form);

      ///don't break since we'll show the clip for editing too.
      //break; 
    case 'view':
      //mode==view/create/edit
      //self.tracker[annID] = new Clipper(annID, dom, hint, data);
      if (data  && data.quote !== null) {
	var wr = new WordRange();
	var str = (data.startPos == Math.floor(data.startPos)) ?
	  String(data.startPos.toFixed(1))
	  :data.startPos;
	str += ' ';
	str += (data.endPos == Math.floor(data.endPos)) ?
	  String(data.endPos.toFixed(1))
	  :data.endPos;
	
	wr.fromString(str,dom);
	wr.container = dom;

	var a = new Annotation();
	a.id = annID;
	a.range = wr;
	a.quote = data.quote;
	a.post = post;
	post.showHighlight(a);
      }
      break;
    }
}

TextAnnotator.prototype.stop = function(annid) {
    var self = this;
    self.tracker[annID].destroy();

}

TextAnnotator.prototype.getValues = function(annid) {

}

TextAnnotator.prototype.update = function(annid, data) {

}

///@return boolean
TextAnnotator.prototype.annotatable = function(dom) {
    return (jQuery('p',dom).length > 0);
}

/*schema(types)
  @param types object with types.dict, types.string, types.arrayOf, etc.
  @return {
      'form':form obj (maybe by classes
      'dict':obj with types.* as pointers with vars
      'indexable_fields':array of strings in dict that are indexable
  }
*/
TextAnnotator.prototype.schema = function(types) {

}

TextAnnotator.prototype._getFormFields = function(form) {
  var rv = {
    grab:jQuery('.textannotationGrab',form).get(0),
    start:jQuery('.textannotationStartPos',form).get(0),
    end:jQuery('.textannotationEndPos',form).get(0),
    quote:jQuery('.textannotationCapturedText',form).get(0)
  };
  return rv;
};

TextAnnotator.notify = function(txt) {
  alert(txt);
};

TextAnnotator._createAnnotation = function(evt,element,post) {
  ///pared down version of createAnnotation(postId,warn) in geof/annotation.js
  ///consider indentation like a block quote :-)
  var warn = true;
	// Test for selection support (W3C or IE)
	if ( ( ! window.getSelection || null == window.getSelection().rangeCount )
		&& null == document.selection )
	{
		if ( warn )
			TextAnnotator.notify( getLocalized( 'browser support of W3C range required for annotation creation' ) );
		return false;
	}

	var range = getPortableSelectionRange();
	if ( null == range )
	{
		if ( warn )
			TextAnnotator.notify( getLocalized( 'select text to annotate' ) );
		return false;
	}
  /*skipping a bunch of code from annotation.js...*/
  var annotation = annotationFromTextRange( post, range );
  /*skipping userid stuff*/
	if ( null == annotation )
	{
		if ( warn )
			TextAnnotator.notify( getLocalized( 'invalid selection' ) );
		return false;
	}
	
	// Check to see whether the quote is too long (don't do this based on the raw text 
	// range because the quote strips leading and trailing spaces)
	if ( annotation.quote.length > MAX_QUOTE_LENGTH )
	{
		annotation.destruct( );
		if ( warn )
			TextAnnotator.notify( getLocalized( 'quote too long' ) );
		return false;
	}
  /* skipping post.createAnnotation() call to procede to micro clip
   * eventually, we'll be calling AnnotationController.update() here?
   * (which would kinda do the same thing)
   */  
  //return true;
  ///since we're not calling post.createAnnotation(annotation), the caller
  ///needs the new annotation object (or what's the point!)
	/*from PostMicro.prototype.removeAnnotations() */  
	stripMarkup( post.contentElement, 'em', AN_HIGHLIGHT_CLASS );
	portableNormalize( post.contentElement );
	removeClass( post.element, AN_ANNOTATED_CLASS );

  post.showHighlight(annotation);
  return annotation;
};

/*
 * override of geof/post-micro.js
 */
function PostMicro(element) {
  this.element = element;
  //the difference in the post-micro implementation is that
  //contentElement is presumably a child of element sans meta-data
  this.contentElement = element;
}

/*
 * Called by annotations, replaced by version in geof/smartcopy.js
 */
function _skipSmartcopy( node )
{
  return (typeof(node.className) == 'string' ) ? hasClass(node, 'ann-ignore' ) : false;
}


/************************************************
 *
 * INITIALIZATION
 *
 ************************************************/

//REGISTER
if (typeof(AnnotationController) != 'undefined') {
    //this might be clobbering.  should do this more conditionally
    AnnotationController.annotators['textannotation'] = new TextAnnotator();
}
