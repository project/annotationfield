///set by annotator JS file loaded by annotator PHP
//eventually part of AnnotationController
var annotationfield_annotators={};


///afID:'annotationfield ID'--a unique (numeric) ID representing the field on the page.
///       this annID is not related to its node or multiple-field index: JUST THE PAGE
//value may eventually be an array so multiple annotators/field are ok
var annotationfield_fields={}; //dict annID=key, value:{annotator,mode,}
/*  annotationfield_field[$afID] = 
    
*/
if (typeof(AnnotationFieldController) == 'undefined') {
    function AnnotationFieldController() {
	this.__NAME__='AnnotationFieldController';
	this.annotators = {};
	this.fields = {};
	this.annIDs = {};
	this.annotatees = {};
	this.annIDcounter = 0;
	return this;
    }
}
    AnnotationFieldController.prototype.getObjects = function(ator) {

    }
    AnnotationFieldController.prototype.getFields = function(ator, objID) {

    }
    AnnotationFieldController.prototype.getObjectDOM = function(ator, objID) {

    }
    AnnotationFieldController.prototype.getFieldDOM = function(ator, objID, afID) {
	
    }
    ///requested by AnnotationField
    AnnotationFieldController.prototype.getAnnotationID = function(afield, ator, atee) {
	var self = this;
	var annID = ++self.annIDcounter;
	self.annIDs[annID] = {'field':afield,'annotator':ator,'annotatee':atee};
	return annID;
    }
    AnnotationFieldController.prototype.updateAnnotationField = function(annID, objID) {

    }

    AnnotationFieldController.prototype.initField = function(mode, afID, annotatee, data, options,
							     /*only for view-mode*/ annotator, fieldname) {
	var self = this;
	switch (mode) {
	case 'create':
	case 'edit':
	    self.fields[afID] = new AnnotationField(mode, afID, jQuery(annotatee).get(0), data);
	    break;
	case 'view':
	    if (annotator && data) {
		var ator = self.annotators[annotator];
		//TODO:maybe introspect annotatee for fieldname here
		var atee = jQuery(annotatee).get(0);
		var annID = self.getAnnotationID(null, ator, atee);
		ator.start(annID, mode, atee, options, data);
	    }
	}
    }

    //AnnotationController = new AnnotationFieldController();
    


///until I figure out where/how this lives
var AnnotationController = new AnnotationFieldController(); //needs to be out of if() for IE
var ac = AnnotationController; //DEBUG

if (typeof(AnnotationField) == 'undefined') {

    function AnnotationField() {
	this.__NAME__='AnnotationField';
	this.potential_annotatees = {};
	this.init.apply(this,arguments);
	//var videoannotation_tracker = {};
	
    }
}
/* 
   @param mode 'create','edit','view'  if not included, expect some other user-action to trigger annotationfield_widget
   @param afID unique ID for the annotationfield on the page
   @param annotatee CSS selector/jQuery obj for what will be annotated
 */
AnnotationField.prototype.init = function(mode, afID, annotatee, data, options) {
    var self = this;
    var ator;
    //0. this might never get called for 'view' mode--just go straight to the annotator?
    //1. learn about self: values, fieldname, etc.

    //2. Either:
    //   a. setup target on annotatee
    self.form_elt = self.getForm(afID);
    if (annotatee && self.form_elt) {
	//  i. get vals from form
	var fields = self.getFormVals(self.form_elt);
	
	// ii. find annotator
	if (fields['annotator']) {
	    ator = AnnotationController.annotators[fields['annotator']]; //also for View
	}
	else {
	    var ator_list = self.findAnnotators(annotatee);
	    switch (ator_list.length) {
	    case 0:
		logDebug('no valid annotators');
		return;
		///NOW:should quit at this point--or degrade gracefully
		break;
	    case 1:
		ator = ator_list[0];
		///NOW: set the annotator value form element.
	    default:
		///FUTURE:offer choices
	    }
	}
	// iii. get the form for the annotator
	var ator_form = self.getAnnotatorFormSegment(fields['annotator']);
	
	// maybe I should save this? why?
	var annID = AnnotationController.getAnnotationID(self, ator, annotatee); //also for View

	//  iv. call the annotator
	//  .start(annID, mode, annotatee, hint, data, ator_form);
	ator.start(annID, mode, annotatee, options, data, ator_form); //also for View
    }
    else {
	//only do this when we actually support multi-field multi-target
    }
}

AnnotationField.prototype.findAnnotators = function(atee) {
    var rv = [];
    var ators = AnnotationController.annotators;
    for (a in ators) {
	if (ators[a].annotatable(jQuery(atee).get(0))) {
	    ///TODO: should also check if we have a form to store the vals in
	    rv.push(ators[a]);
	}
    }
    return rv;
}

/*
 * Drupal-specific setup
 */

AnnotationField.prototype.getForm = function(afID) {
    return jQuery('.annotationfield-'+afID).get(0);
}

AnnotationField.prototype.getFormVals = function(form) {
    return {'annotator':jQuery('.annotationfield-annotator',form).attr('value'),
		'fielditem':jQuery('.annotationfield-fielditem',form).attr('value'),
		'fieldname':jQuery('.annotationfield-fieldname',form).attr('value')
		};
}

AnnotationField.prototype.getAnnotatorFormSegment = function(ator_type) {
    var self = this;
    return jQuery('.'+ator_type, self.form_elt).get(0);
}


/****************************************************
 * Debugging, since jQuery is not as cool as MochiKit
 ****************************************************/
function joinArgs(str,args) {
  var s = '';
  for(var i=0;i<args.length;i++) {
    if (typeof(args[i]) =='string') {
      s += args[i] + s;
    }
    else {
      myLog(args[i]);
    }
  }
  return s;
}
function myLog(x) {
    try {
	window.console.log(x);
    } catch(e) {
	/*no window.console.log available, i guess? */
    }
}

logDebug = function(x,y){myLog(joinArgs('||',arguments));};


