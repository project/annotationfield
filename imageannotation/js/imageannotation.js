/*
 registration to AnnotationController.annotators['imageannotation']
 is at the bottom of the file
*/
//logDebug = MochiKit.Logging.logDebug;

if (typeof(ImageAnnotator) == 'undefined') {

    function ImageAnnotator() {
	this.__NAME__='ImageAnnotator';
	this.tracker = {};
	//var videoannotation_tracker = {};
    }
    function ImageAnnotation(data) {
	this.center = {};
	this.dimensions = {};

	this.zoom = data.zoom;
	this.center.x = data.center.x;
	this.center.y = data.center.y;
	this.dimensions.w = data.dimensions.w;
	this.dimensions.h = data.dimensions.h;
	
	this.panzoomer = null;
	this.annotationForm = null;
	this.annID = null;
    }
}

ImageAnnotator.images = [];

ImageAnnotator.prototype.start = function(annID, mode, dom, hint, data, form) {
    var self = this;
    var logDebug = MochiKit.Logging.logDebug;
    switch (mode) {
    case 'create':
    case 'edit':
      //TODO:currently assumes this will be run before microformatPanZoom()
      var az = self.tracker[annID] = self._buildUp(dom,data);
      az.annID = annID;
      az.annotatorForm = self._getFormFields(form);
	
      MochiKit.Signal.connect(az.annotatorForm.grab,'onclick',az,'updateAnnotatorForm');
      //MochiKit.Signal.connect(az.panzoomer,'frame_update',az,'updateAnnotatorForm');

      //self.tracker[annID] = new Clipper(annID, dom, hint, data, form);
      break;
      // 3. roll up the second 'set end'? (optional for now?, since not in HHP comps)
    case 'view':
      self.tracker[annID] = self._buildUp(dom,data);
      self.tracker[annID].annID = annID;
      //mode==view/create/edit
      // 4. make clip bar (JS jumps to start and plays when clicking the internal div)
      //self.tracker[annID] = new Clipper(annID, dom, hint, data);
      break;
    }

};

ImageAnnotator.prototype.stop = function(annid) {
    var self = this;
    self.tracker[annID].destroy();

};

ImageAnnotator.prototype.getValues = function(annid) {

};

ImageAnnotator.prototype.update = function(annid, data) {

};

///@return boolean
ImageAnnotator.prototype.annotatable = function(dom) {
    return (jQuery('img',dom).length > 0);
};

/*schema(types)
  @param types object with types.dict, types.string, types.arrayOf, etc.
  @return {
      'form':form obj (maybe by classes
      'dict':obj with types.* as pointers with vars
      'indexable_fields':array of strings in dict that are indexable
  }
*/
ImageAnnotator.prototype.schema = function(types) {

};

ImageAnnotator.prototype._getFormFields = function(form) {
  var rv = {
    x:jQuery('.imageannotationCenterX',form).get(0),
    y:jQuery('.imageannotationCenterY',form).get(0),
    zoom:jQuery('.imageannotationZoom',form).get(0),
    w:jQuery('.imageannotationWidth',form).get(0),
    h:jQuery('.imageannotationHeight',form).get(0),
    //,alert:jQuery('.imageannotationUpdate',form).get(0)
    grab:jQuery('.imageannotationGrab',form).get(0)
  };
  return rv;
};

ImageAnnotator.prototype._buildUp = function(dom,data) {
    var controls = {};
    /*TODO: this should be tucked into ImageAnnotation
      or somewhere that doesn't decorate an un-selected frame
      by default.
     */
    data = (data)?data:{center:{'x':0.5,'y':0.5},
			dimensions:{w:1,h:1},
			zoom:1
    };
    var imgs = jQuery('img',dom)
    //.addClass('panzoom')
    .wrap('<div class="imagecontainer" style="position:relative;display:block;overflow:hidden;cursor:move;"></div>');

    /*TODO:For now just the first image.  
     * In the future, maybe images are considered items?
     */
    var i_annotation = new ImageAnnotation(data);
    sky = i_annotation;
    var victim = imgs.get(0); 

    var idmatch = String(victim.parentNode.id).match(/^panzoomer-(\d+)/);
    var id = ImageAnnotator.images.length;
    if (idmatch !== null) {
	id = parseInt(idmatch[1]);
	pz = ImageAnnotator.images[id];
    }
    else {
	jQuery(victim)
	.wrap('<div id="panzoomer-'+id+'" class="imagecontainer"></div>')
	.wrap('<div class="panzoom-imager"></div>');
	var d = {'w':victim.width,
	         'h':victim.height
		};
	var container = victim.parentNode.parentNode;

	container.style.width = d.w +'px';
	container.style.height = d.h +'px';
	container.style.clip = 'rect(0px,'+d.w+'px,'+d.h+'px,0px)';
	jQuery(container).before('<div class="image-controls"><div class="zoom" id="panzoomer-'+id+'_zoomIn">+</div><div class="zoom" id="panzoomer-'+id+'_zoomOut">&minus;</div></div>');

	i_annotation.imgdom = victim.parentNode;
	
	var control_dom = jQuery(container).prev('.image-controls').get(0);
	control_dom;
	controls.zoomin = control_dom.childNodes[0];
	controls.zoomout = control_dom.childNodes[1];
	
	pz = new PanZoom(controls,i_annotation);

	pz.imager = new Imager(pz);

	pz.imager.swapImages([victim]);

	ImageAnnotator.images[id] = pz;
    }

    var image_wrapper = victim.parentNode;    
    jQuery(image_wrapper).append('<div class="image-annotation-selection"></div>');
    i_annotation.dom = image_wrapper.lastChild;

    pz.annotate(i_annotation);

    i_annotation.panzoomer = pz;
    return i_annotation;
};

/********************
 * PanZoom addendum
 * -we could sub-class or something, but this is javascript!
 ********************/

ImageAnnotation.prototype.updateAnnotatorForm = function() {
    var self = this;
    var f = self.annotatorForm;
    var pz = self.panzoomer;
    f.x.value = self.center.x = pz.center.x;
    f.y.value = self.center.y = pz.center.y;
    f.zoom.value = self.zoom = pz.zoom;
    f.w.value = self.dimensions.w = pz.dimensions.w;
    f.h.value = self.dimensions.h = pz.dimensions.h;
    
    MochiKit.Signal.signal(self,'frame_update');

    //MochiKit.Style.showElement(f.alert);
    //MochiKit.Visual.fade(f.alert, {from:1});
  
};


/************************************************
 *
 * INITIALIZATION
 *
 ************************************************/

//REGISTER
if (typeof(AnnotationController) != 'undefined') {
    //this might be clobbering.  should do this more conditionally
    AnnotationController.annotators['imageannotation'] = new ImageAnnotator();
}
